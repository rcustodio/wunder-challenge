#!/bin/sh

# cwd
cd /var/www/wunder-php

# composer install if vendors dir doesn't exist
if [[ ! -f vendor/autoload.php ]]; then

  composer install

fi

# run server
php -S 0.0.0.0:9080 -t public/ 2>&1