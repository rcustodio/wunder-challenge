#!/bin/sh

# cwd
cd /var/www/wunder-node

# yarn install if node_modules dir doesn't exist
if [[ ! -d node_modules ]]; then

  yarn install

fi

# run server
yarn start