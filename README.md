# Wunder Challenge #

### Requirements ###

* Docker
* Ports 80, 3000, 3306 not being used to run the containers

### How do I get set up? ###

* Execute `docker compose up -d --build`
* Execute `docker exec -it wunder-challenge_api_1 php bin/console doctrine:migrations:migrate`
* Add `NETWORK_IP wunder-challenge.local` to your `/etc/hosts`
* Access via `http://wunder-challenge.local/`

### Testing ###

* Execute `docker exec -e APP_ENV=test -it wunder-challenge_api_1 vendor/bin/phpunit -c phpunit.xml` to run phpunit tests

### Description ###

I'm a full-stack developer more specialised on back-end.

I have use Symfony on back-end as a RESTful API and React with Material UI on the front-end for the wizard form. The filled data is being kept in the browser storage until the customer ends all steps. I have added testing to the PHP using PHPUnit but not on the front-end as I'm not familiar with front-end testing. The customer data could be saved on the database since the first step, giving leads to the sales department but that might also break some law, depending on the country, so I chose not to.

Having more time I would have added validators on the back-end and also studied more about front-end testing to add it to the project.