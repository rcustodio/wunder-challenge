import Logo from './logo.svg';
import './App.css';
import Wizard from "./Components/Wizard";

function App() {
  return (
    <div className="App">
      <img src={Logo} height={120} alt="Wunder Mobility" />
      <Wizard></Wizard>
    </div>
  );
}

export default App;
