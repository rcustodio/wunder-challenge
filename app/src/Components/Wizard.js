
import { makeStyles } from '@material-ui/core/styles';
import { Button, CircularProgress, Step, StepLabel, Stepper, Typography } from "@material-ui/core";
import Axios from 'axios';
import { useState } from 'react';
import Address from "./Wizard/Address";
import Payment from "./Wizard/Payment";
import Personal from "./Wizard/Personal";

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return ['Personal data', 'Address', 'Payment'];
}

function getStepContent(step, setDisableNext) {
  switch (step) {
    case 0:
      return (<Personal setDisableNext={setDisableNext} />);
    case 1:
      return (<Address setDisableNext={setDisableNext} />);
    case 2:
      return (<Payment setDisableNext={setDisableNext} />);
    default:
      return 'Unknown step';
  }
}

export default function Wizard() {
  const classes = useStyles();
  const wizardStep = parseInt(localStorage.getItem("wizardStep") || 0);
  const [activeStep, setActiveStep] = useState(wizardStep);
  const [disableNext, setDisableNext] = useState(true);
  const [submitting, setSubmitting] = useState(false);
  const [paymentId, setPaymentId] = useState("");
  const steps = getSteps();

  const handleNext = async (prevActiveStep) => {
    const nextStep = prevActiveStep + 1;
    setDisableNext(true)


    if (prevActiveStep === steps.length - 1) {
      const formData = JSON.parse(localStorage.getItem("wizardFormData"));
      setSubmitting(true);

      const response = await Axios.post("/api/customer", formData);
      setPaymentId(response.data.paymentId);

      localStorage.removeItem("wizardStep");
      localStorage.removeItem("wizardFormData");
      setSubmitting(false);
    } else {
      localStorage.setItem("wizardStep", nextStep);
    }

    setActiveStep(nextStep);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => {
      setDisableNext(true);
      localStorage.setItem("wizardStep", prevActiveStep - 1);
      return prevActiveStep - 1;
    });
  };

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep}>
        {steps.map((label, index) => {
          const stepProps = {};
          const labelProps = {};
          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          );
        })}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>
              Thank you for submitting your data, payment { paymentId }
            </Typography>
          </div>
        ) : (
          <div>
            <div className={classes.instructions} style={{marginLeft: 40, marginRight: 40}}>
              {getStepContent(activeStep, setDisableNext)}
            </div>
            <div>
              <Button disabled={activeStep === 0 || submitting} onClick={handleBack} className={classes.button}>
                Back
              </Button>

              <Button
                variant="contained"
                color="primary"
                onClick={() => handleNext(activeStep)}
                className={classes.button}
                disabled={disableNext || submitting}
              >
                {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                {submitting && <CircularProgress color="primary" size={12} style={{marginLeft: 5}} />}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}