import 'react-phone-number-input/style.css';
import { FormControl, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';
import { Language } from '@material-ui/icons';
import { forwardRef, useEffect, useState } from 'react';
import PhoneInput, { isValidPhoneNumber } from 'react-phone-number-input';

const PhoneTextField = forwardRef((props, ref) => {
  return (<TextField {...props} label="Telephone" inputRef={ref}/>);
});

const saveData = (formData, firstName, lastName, phone) => {
  if (!formData.personal) { formData.personal = {} };
  formData.personal.firstName = firstName;
  formData.personal.lastName = lastName;
  formData.personal.phone = phone;
  localStorage.setItem("wizardFormData", JSON.stringify(formData));
};

export default function Personal({setDisableNext}) {
  const formData = JSON.parse(localStorage.getItem("wizardFormData") || "{}");
  const [firstName, setFirstName] = useState(formData?.personal?.firstName || "");
  const [lastName, setLastName] = useState(formData?.personal?.lastName || "");
  const [phone, setPhone] = useState(formData?.personal?.phone || "");
  const [errors, setErrors] = useState({});

  useEffect(() => {
    if (Object.keys(errors).length > 0) {
      setDisableNext(true);
      return;
    }

    if (!validateName(firstName, "firstName", false)) {
      setDisableNext(true);
      return;
    }
    saveData(formData, firstName, lastName, phone);

    if (!validateName(lastName, "lastName", false)) {
      setDisableNext(true);
      return;
    }
    saveData(formData, firstName, lastName, phone);

    if (!validatePhone(phone, "phone", false)) {
      setDisableNext(true);
      return;
    }
    saveData(formData, firstName, lastName, phone);

    setDisableNext(false);
  }, [firstName, lastName, phone, errors]);

  const validateName = (value, field, showError = true) => {
    if (!value || value.length < 3) {
      showError && setErrors(
        errors => ({...errors, [field]: `The ${field === "firstName" ? "first" : "last"} name is required and should have at least 3 characters`})
      );
      return false;
    }

    setErrors(errors => {
      delete errors[field];
      return errors;
    });
    return true;
  };

  const validatePhone = (phone, showError = true) => {
    if (!isValidPhoneNumber(phone || "")) {
      showError && setErrors(errors => ({...errors, phone: "The phone number is invalid"}));
      return false;
    }

    setErrors(errors => {
      delete errors['phone'];
      return errors;
    });
    return true;
  };

  return (<FormControl fullWidth>
    <TextField
      label="First name"
      value={firstName}
      onChange={(event) => {
        validateName(event.target.value, "firstName");
        return setFirstName(event.target.value);
      }}
      error={'firstName' in errors}
      helperText={errors.firstName && errors.firstName}
    />

    <TextField
      label="Last name"
      value={lastName}
      onChange={(event) => {
        validateName(event.target.value, "lastName");
        return setLastName(event.target.value);
      }}
      error={'lastName' in errors}
      helperText={errors.lastName && errors.lastName}
    />

    <PhoneInput
      international
      defaultCountry="DE"
      value={phone}
      onChange={phone => {
        validatePhone(phone);
        return setPhone(phone);
      }}
      inputComponent={PhoneTextField}
      error={'phone' in errors}
      helperText={errors.phone && errors.phone}
    />
  </FormControl>);
}