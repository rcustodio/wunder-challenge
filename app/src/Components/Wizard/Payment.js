import { FormControl, TextField } from '@material-ui/core';
import { useEffect, useState } from 'react';

const saveData = (formData, iban, owner) => {
  if (!formData.payment) { formData.payment = {} };
  formData.payment.iban = iban;
  formData.payment.owner = owner;
  localStorage.setItem("wizardFormData", JSON.stringify(formData));
};

export default function Payment({setDisableNext}) {
  const formData = JSON.parse(localStorage.getItem("wizardFormData") || "{}");
  const [iban, setIBAN] = useState(formData?.payment?.iban || "");
  const [owner, setOwner] = useState(formData?.payment?.owner || "");
  const [errors, setErrors] = useState({});

  useEffect(() => {
    if (Object.keys(errors).length > 0) {
      setDisableNext(true);
      return;
    }

    if (!validateRequired(iban, "iban", false)) {
      setDisableNext(true);
      return;
    }
    saveData(formData, iban, owner);

    if (!validateRequired(owner, "owner", false)) {
      setDisableNext(true);
      return;
    }
    saveData(formData, iban, owner);

    setDisableNext(false);
  }, [iban, owner, errors]);

  const validateRequired = (value, field, showError = true) => {
    if (!value || value.length < 3) {
      showError && setErrors(
        errors => ({...errors, [field]: `The ${field.replace(/([A-Z]+)/g, " $1").toLowerCase()} is required and should have at least 3 characters`})
      );
      return false;
    }

    setErrors(errors => {
      delete errors[field];
      return errors;
    });
    return true;
  };

  return (<FormControl fullWidth>
    <TextField
      label="IBAN"
      value={iban}
      onChange={(event) => {
        validateRequired(event.target.value, "iban");
        return setIBAN(event.target.value);
      }}
      error={'iban' in errors}
      helperText={errors.iban && errors.iban}
    />
    <TextField
      label="Owner"
      value={owner}
      onChange={(event) => {
        validateRequired(event.target.value, "owner");
        return setOwner(event.target.value);
      }}
      error={'owner' in errors}
      helperText={errors.owner && errors.owner}
    />
  </FormControl>);
}