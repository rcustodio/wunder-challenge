import { FormControl, Grid, TextField } from '@material-ui/core';
import { forwardRef, useEffect, useState } from 'react';

const saveData = (formData, additionalInfo, street, number, zipCode, city) => {
  if (!formData.address) { formData.address = {} };
  formData.address.additionalInfo = additionalInfo;
  formData.address.street = street;
  formData.address.number = number;
  formData.address.zipCode = zipCode;
  formData.address.city = city;
  localStorage.setItem("wizardFormData", JSON.stringify(formData));
};

export default function Address({setDisableNext}) {
  const formData = JSON.parse(localStorage.getItem("wizardFormData") || "{}");
  const [additionalInfo, setAdditionalInfo] = useState(formData?.address?.additionalInfo || "");
  const [street, setStreet] = useState(formData?.address?.street || "");
  const [number, setNumber] = useState(formData?.address?.number || "");
  const [zipCode, setZipCode] = useState(formData?.address?.zipCode || "");
  const [city, setCity] = useState(formData?.address?.city || "");
  const [errors, setErrors] = useState({});

  useEffect(() => {
    if (Object.keys(errors).length > 0) {
      setDisableNext(true);
      return;
    }

    if (!validateRequired(street, "street", 3, false)) {
      setDisableNext(true);
      return;
    }
    saveData(formData, additionalInfo, street, number, zipCode, city);

    if (!validateRequired(number, "number", 1, false)) {
      setDisableNext(true);
      return;
    }
    saveData(formData, additionalInfo, street, number, zipCode, city);

    if (!validateRequired(zipCode, "zipCode", 5, false)) {
      setDisableNext(true);
      return;
    }
    saveData(formData, additionalInfo, street, number, zipCode, city);

    if (!validateRequired(city, "city", 3, false)) {
      setDisableNext(true);
      return;
    }
    saveData(formData, additionalInfo, street, number, zipCode, city);

    setDisableNext(false);
  }, [additionalInfo, street, number, zipCode, city, errors]);

  const validateRequired = (value, field, min = 3, showError = true) => {
    if (!value || value.length < min) {
      showError && setErrors(
        errors => ({...errors, [field]: `The ${field.replace(/([A-Z]+)/g, " $1").toLowerCase()} is required and should have at least ${min} characters`})
      );
      return false;
    }

    setErrors(errors => {
      delete errors[field];
      return errors;
    });
    return true;
  };

  return (<FormControl fullWidth>
    <TextField
      label="Additional info"
      value={additionalInfo}
      onChange={(event) => setAdditionalInfo(event.target.value)}
    />

    <Grid container spacing={3}>
      <Grid item xs={10}>
        <TextField
          fullWidth
          label="Street"
          value={street}
          onChange={(event) => {
            validateRequired(event.target.value, "street");
            return setStreet(event.target.value);
          }}
          error={'street' in errors}
          helperText={errors.street && errors.street}
        />
      </Grid>
      <Grid item xs={2}>
        <TextField
          fullWidth
          label="Nº"
          value={number}
          onChange={(event) => {
            validateRequired(event.target.value, "number", 1);
            return setNumber(event.target.value);
          }}
          error={'number' in errors}
          helperText={errors.number && errors.number}
        />
      </Grid>
    </Grid>

    <Grid container spacing={3}>
      <Grid item xs={2}>
        <TextField
          fullWidth
          label="Zip code"
          value={zipCode}
          onChange={(event) => {
            validateRequired(event.target.value, "zipCode", 5);
            return setZipCode(event.target.value);
          }}
          error={'zipCode' in errors}
          helperText={errors.zipCode && errors.zipCode}
        />
      </Grid>
      <Grid item xs={10}>
        <TextField
          fullWidth
          label="City"
          value={city}
          onChange={(event) => {
            validateRequired(event.target.value, "city");
            return setCity(event.target.value);
          }}
          error={'city' in errors}
          helperText={errors.city && errors.city}
        />
      </Grid>
    </Grid>
  </FormControl>);
}