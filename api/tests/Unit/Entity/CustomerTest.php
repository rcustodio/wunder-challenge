<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Customer;
use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{
    public function testCustomerCreation(): void
    {
        $customer = new Customer(1);
        $customer->setFirstName("John");
        $customer->setLastName("Doe");
        $customer->setTelephone("+49152259595");

        $customer->setStreet("Testing street");
        $customer->setHouseNo("123");
        $customer->setZipCode("20000");
        $customer->setCity("Hamburg");

        $customer->setIban("DE37481728495817");
        $customer->setOwner("John Doe");

        $this->assertInstanceOf(Customer::class, $customer);

        $this->assertEquals($customer->getFirstName(), "John");
        $this->assertEquals($customer->getLastName(), "Doe");
        $this->assertEquals($customer->getTelephone(), "+49152259595");

        $this->assertEquals($customer->getStreet(), "Testing street");
        $this->assertEquals($customer->getHouseNo(), "123");
        $this->assertEquals($customer->getZipCode(), "20000");
        $this->assertEquals($customer->getCity(), "Hamburg");
        $this->assertNull($customer->getAddressAdditional());

        $this->assertEquals($customer->getIban(), "DE37481728495817");
        $this->assertEquals($customer->getOwner(), "John Doe");
    }
}