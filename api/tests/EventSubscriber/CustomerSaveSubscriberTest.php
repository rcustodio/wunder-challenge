<?php

namespace App\Tests\EventSubscriber;

use App\Entity\Customer;
use App\EventSubscriber\CustomerSaveSubscriber;
use App\Repository\Event\CustomerSaveEvent;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CustomerSaveSubscriberTest extends KernelTestCase
{
    public function testSavePaymentDataId(): void
    {
        static::bootKernel();
        $client = static::getContainer()->get("http_client");
        $logger = static::getContainer()->get("logger");

        $customer = new Customer(1);
        $customer->setFirstName("John");
        $customer->setLastName("Doe");
        $customer->setTelephone("+49152259595");
        $customer->setStreet("Testing street");
        $customer->setHouseNo("123");
        $customer->setZipCode("20000");
        $customer->setCity("Hamburg");
        $customer->setIban("DE37481728495817");
        $customer->setOwner("John Doe");

        $event = new CustomerSaveEvent($customer);
        $subscriber = new CustomerSaveSubscriber($client, $logger);
        $subscriber->onCustomerSave($event);

        static::assertNotEmpty($customer->getPaymentId());
    }
}