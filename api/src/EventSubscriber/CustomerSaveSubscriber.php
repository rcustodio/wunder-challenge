<?php

namespace App\EventSubscriber;

use App\Repository\Event\CustomerSaveEvent;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CustomerSaveSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private HttpClientInterface $httpClient,
        private LoggerInterface $logger
    )
    {

    }
    public static function getSubscribedEvents()
    {
        return [
            CustomerSaveEvent::NAME => 'onCustomerSave'
        ];
    }

    public function onCustomerSave(CustomerSaveEvent $event)
    {
        $customer = $event->getCustomer();

        try {
            $response = $this->httpClient->request(
                'POST',
                'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data',
                [
                    'json' => [
                        'customerId' => $customer->getId(),
                        'iban' => $customer->getIban(),
                        'owner' => $customer->getOwner(),
                    ]
                ]
            );
            $body = json_decode($response->getContent(), true);

            $customer->setPaymentId($body['paymentDataId']);
        } catch (Exception | TransportExceptionInterface $e) {
            $this->logger->warning($e->getMessage());
        }
    }
}