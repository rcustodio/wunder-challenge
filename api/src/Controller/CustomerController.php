<?php

namespace App\Controller;

use App\Repository\CustomerRepository;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use TypeError;

class CustomerController extends AbstractController
{
    public function __construct(
        private CustomerRepository $repository,
        private LoggerInterface $logger,
        private SerializerInterface $serializer
    )
    {
    }

    #[Route('/customer', name: 'customer', methods: ['GET'])]
    public function index(): JsonResponse
    {
        return new JsonResponse([], Response::HTTP_OK);
    }

    #[Route('/customer', name: 'customer', methods: ['POST', 'PUT'])]
    public function save(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        try {
            $customer = $this->repository->save($data);
            return new JsonResponse(
                json_decode($this->serializer->serialize($customer, 'json'), true),
                Response::HTTP_OK
            );
        } catch (TypeError $e) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return new JsonResponse([], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
