<?php

namespace App\Repository\Event;

use App\Entity\Customer;
use Symfony\Contracts\EventDispatcher\Event;

class CustomerSaveEvent extends Event
{
    public const NAME = "customer.save";

    public function __construct(private Customer $customer)
    {
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }
}