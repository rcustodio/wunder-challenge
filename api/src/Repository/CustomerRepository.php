<?php

namespace App\Repository;

use App\Entity\Customer;
use App\Repository\Event\CustomerSaveEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use TypeError;

/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private EventDispatcherInterface $dispatcher,
        private LoggerInterface $logger
    )
    {
        parent::__construct($registry, Customer::class);
    }

    public function save(array $data): ?Customer
    {
        $customer = new Customer();
        $customer->setFirstName($data['personal']['firstName']);
        $customer->setLastName($data['personal']['lastName']);
        $customer->setTelephone($data['personal']['phone']);
        $customer->setStreet($data['address']['street']);
        $customer->setHouseNo($data['address']['number']);
        $customer->setZipCode($data['address']['zipCode']);
        $customer->setCity($data['address']['city']);
        $customer->setAddressAdditional($data['address']['additionalInfo']);
        $customer->setIban($data['payment']['iban']);
        $customer->setOwner($data['payment']['owner']);

        $this->getEntityManager()->persist($customer);
        $this->getEntityManager()->flush();

        $event = new CustomerSaveEvent($customer);
        $this->dispatcher->dispatch($event, CustomerSaveEvent::NAME);

        $this->getEntityManager()->persist($customer);
        $this->getEntityManager()->flush();

        return $customer;
    }
}
